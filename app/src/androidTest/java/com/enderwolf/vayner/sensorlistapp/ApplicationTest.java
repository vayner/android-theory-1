package com.enderwolf.vayner.sensorlistapp;

import android.app.Application;
import android.test.ApplicationTestCase;

import junit.framework.Assert;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
       super(Application.class);

        /* Cant be tested, depends on real world framework
        Assert.assertEquals(
                "Sensormanager is not consistent within the same context",
                SensorList.getSensorManager(mContext),
                SensorList.getSensorManager(mContext)
        );

        Assert.assertEquals(
                "List ovet sensors are not consistent within the same context",
                SensorList.getSensors(mContext),
                SensorList.getSensors(mContext)
        );
        */
    }
}