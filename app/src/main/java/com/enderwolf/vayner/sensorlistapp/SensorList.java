package com.enderwolf.vayner.sensorlistapp;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.util.Log;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * Created by tellef on 13.09.2014.
 */
public class SensorList {

    private static ArrayList<SensorItem> deviceSensors = null;
    private static SensorManager sensorManager = null;

    private static HashMap<Integer, String> SensorTypeMap = new HashMap<Integer, String>();

    static {

        Field [] unsortedFields = Sensor.class.getDeclaredFields();
        Vector<Field> intTypes = new Vector<Field>(20);

        for (Field f : unsortedFields) {
            if (f.getName().startsWith("TYPE_")) {
                intTypes.add(f);
            }
        }

        for (Field intField : intTypes) {
            try {
                SensorTypeMap.put(
                    intField.getInt(Sensor.class),
                        // formats sensor type string   TYPE_LIGHT -> Light    TYPE_GRAVITY -> Gravity    etc...
                        intField.getName().substring(5).charAt(0) + intField.getName().substring(5).toLowerCase().replace('_', ' ').substring(1)
                );
            } catch (IllegalAccessException e) {
                Log.wtf("Reflection", "Failed reflextion attemt for sensor name lookup");
            }
        }
    }

    public static ArrayList<SensorItem> getSensors(Context context) {
        if(deviceSensors != null && sensorManager != null) {
            return deviceSensors;
        }

        List<Sensor> sensors = getSensorManager(context).getSensorList(Sensor.TYPE_ALL);
        deviceSensors = new ArrayList<SensorItem>();

        for (Sensor s : sensors) {
            deviceSensors.add(new SensorItem(s));
        }

        return deviceSensors;
    }

    public static SensorManager getSensorManager (Context context) {
        if(sensorManager != null) {
            return sensorManager;
        }

        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        return sensorManager;
    }

    public static class SensorItem {
        private Sensor sensor;

        SensorItem (Sensor sensor) {
            this.sensor = sensor;
        }

        public Sensor getSensor() {
            return this.sensor;
        }

        public String getSensorTypeString() {
            return SensorTypeMap.get(this.sensor.getType());
        }

        @Override
        public String toString() {
            return sensor.getName();
        }
    }
}
