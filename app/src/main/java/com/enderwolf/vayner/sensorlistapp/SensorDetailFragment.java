package com.enderwolf.vayner.sensorlistapp;

import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.enderwolf.vayner.sensorlistapp.sensordisplay.AccelerometerData;
import com.enderwolf.vayner.sensorlistapp.sensordisplay.*;


/**
 * A fragment representing a single Sensor detail screen.
 * This fragment is either contained in a {@link SensorListActivity}
 * in two-pane mode (on tablets) or a {@link SensorDetailActivity}
 * on handsets.
 */
public class SensorDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private int sensorIndex = 0;
    private SensorList.SensorItem sensor;
    private SensorData sensorData = null;
    private boolean sensorRegisterd = false;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SensorDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            sensorIndex = getArguments().getInt(ARG_ITEM_ID);
            sensor = SensorList.getSensors(this.getActivity()).get(sensorIndex);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sensor_detail, container, false);

        if(sensor != null) {
            ((TextView) rootView.findViewById(R.id.sensor_name)).setText(sensor.toString());
            ((TextView) rootView.findViewById(R.id.sensor_type)).setText(sensor.getSensorTypeString());
            ((TextView) rootView.findViewById(R.id.sensor_vendor)).setText(sensor.getSensor().getVendor());

            sensorData = new AllSensorData((GridLayout) rootView.findViewById(R.id.sensor_detail_data), this.getActivity());

            sensorRegisterd = SensorList.getSensorManager(this.getActivity()).registerListener(sensorData, sensor.getSensor(), SensorManager.SENSOR_DELAY_UI);
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(ARG_ITEM_ID, sensorIndex);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if(sensorRegisterd == true) {
            SensorList.getSensorManager(this.getActivity()).unregisterListener(sensorData);
            sensorData.cleanup();
            sensorData = null;
            sensorRegisterd = false;
        }
    }
}
