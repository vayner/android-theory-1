package com.enderwolf.vayner.sensorlistapp.sensordisplay;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.view.Gravity;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.enderwolf.vayner.sensorlistapp.R;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by !Tulingen on 16.09.2014.
 */
public class AllSensorData extends SensorData {

    private static final int DATA_FIELDS = 6;
    private TextView [] textViews  = new TextView[DATA_FIELDS];

    public AllSensorData (GridLayout root, Context context) {
        super(root, context);


        for (int i = 0; i < DATA_FIELDS; i++) {
            textViews[i] = new TextView(this.context);
            textViews[i].setText("Test: " + i);

            GridLayout.LayoutParams params = new GridLayout.LayoutParams();
            params.columnSpec = GridLayout.spec(0);
            params.rowSpec = GridLayout.spec(i);

            this.root.addView(textViews[i], params);
        }

        ImageView image = new ImageView(context);

        GridLayout.LayoutParams params = new GridLayout.LayoutParams();
        params.columnSpec = GridLayout.spec(1);
        params.rowSpec = GridLayout.spec(0,DATA_FIELDS);
        params.setGravity(Gravity.RIGHT);

        this.root.addView(image, params);

        // more reliable than just picasso
        OkHttpClient okHttpClient = new OkHttpClient();
        Picasso p = new Picasso.Builder(context).downloader(new OkHttpDownloader(okHttpClient)).build();

        p
            .with(context)
            .load("http://enderwolf.com/icons/ubuntu-logo.png")
            //.placeholder(R.drawable.ic_launcher)
            .error(R.drawable.ic_launcher)
            .into(image);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float [] data = sensorEvent.values;

        int i = 0;
        for (float f : data) {
            textViews[i++].setText(String.valueOf(f));
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
