package com.enderwolf.vayner.sensorlistapp.sensordisplay;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.widget.GridLayout;

/**
 * Created by tellef on 16.09.2014.
 */
public abstract class SensorData implements SensorEventListener{

    protected GridLayout root;
    protected Context context;

    public SensorData (GridLayout root, Context context) {
        this.root = root;
        this.context = context;
    }

    public void cleanup () {
        this.root.removeAllViewsInLayout();
    }
}
