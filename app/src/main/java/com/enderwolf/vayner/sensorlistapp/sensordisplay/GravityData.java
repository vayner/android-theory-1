package com.enderwolf.vayner.sensorlistapp.sensordisplay;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.widget.GridLayout;
import android.widget.TextView;

/**
 * Created by tellef on 16.09.2014.
 */
public class GravityData extends SensorData {

    private TextView x;
    private TextView y;
    private TextView z;

    public GravityData (GridLayout root, Context context) {
        super(root, context);

        x = new TextView(this.context);
        y = new TextView(this.context);
        z = new TextView(this.context);

        GridLayout.LayoutParams paramsX = new GridLayout.LayoutParams();
        paramsX.columnSpec = GridLayout.spec(0);
        paramsX.rowSpec = GridLayout.spec(0);

        this.root.addView(x, paramsX);

        GridLayout.LayoutParams paramsY = new GridLayout.LayoutParams();
        paramsY.columnSpec = GridLayout.spec(1);
        paramsY.rowSpec = GridLayout.spec(0);

        this.root.addView(y, paramsY);

        GridLayout.LayoutParams paramsZ = new GridLayout.LayoutParams();
        paramsZ.columnSpec = GridLayout.spec(2);
        paramsZ.rowSpec = GridLayout.spec(0);

        this.root.addView(z, paramsZ);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float [] data = sensorEvent.values;

        x.setText(String.valueOf(data[0]));
        y.setText(String.valueOf(data[1]));
        z.setText(String.valueOf(data[2]));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

}
