package com.enderwolf.vayner.sensorlistapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Activity;




/**
 * An activity representing a list of Sensors. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link SensorDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link SensorListFragment} and the item details
 * (if present) is a {@link SensorDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link SensorListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class SensorListActivity extends Activity
        implements SensorListFragment.Callbacks {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private static final String LIST_INDEX_KEY = "LIST_INDEX_KEY";
    private int lastIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_list);

        if (findViewById(R.id.sensor_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((SensorListFragment) getFragmentManager().findFragmentById(R.id.sensor_list)).setActivateOnItemClick(true);
        }

        if(savedInstanceState == null) {
            SharedPreferences pref = this.getSharedPreferences(getString(R.string.pref_file), this.MODE_PRIVATE);
            this.onItemSelected(pref.getInt(LIST_INDEX_KEY,0));
        }

        // TODO: If exposing deep links into your app, handle intents here.
    }

    /**
     * Callback method from {@link SensorListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(int index) {
        lastIndex = index;

        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putInt(SensorDetailFragment.ARG_ITEM_ID, index);

            SensorDetailFragment fragment = new SensorDetailFragment();
            fragment.setArguments(arguments);

            getFragmentManager().beginTransaction().replace(R.id.sensor_detail_container, fragment).commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, SensorDetailActivity.class);
            detailIntent.putExtra(SensorDetailFragment.ARG_ITEM_ID, index);
            startActivity(detailIntent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(LIST_INDEX_KEY, lastIndex);
        SharedPreferences pref = this.getSharedPreferences(getString(R.string.pref_file), this.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();

        edit.putInt(LIST_INDEX_KEY, lastIndex);
        edit.commit();

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        lastIndex = savedInstanceState.getInt(LIST_INDEX_KEY);
        this.onItemSelected(lastIndex);

        super.onRestoreInstanceState(savedInstanceState);
    }
}
